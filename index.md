---
layout: default.liquid
---
# {{site.title}}

{% for post in collections.posts.pages %}
[{{post.title}}]({{post.permalink}})
{% endfor %}
